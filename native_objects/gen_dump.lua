--
-- dump records
--
print"============ Dump records ================="
local depth=0
function write(...)
    io.write(("  "):rep(depth))
    io.write(...)
end

process_records{
unknown = function(self, rec, parent)
    write(rec._rec_type .. " {\n")
    depth = depth + 1
    -- dump rec info
    for k,v in pairs(rec) do
        if k == '_rec_type' then
        elseif k == 'c_type_rec' then
            write(k,' = ', tostring(v._rec_type), ' = {\n')
            depth = depth + 1
            write('name = "', tostring(v.name), '"\n')
            write('c_type = "', tostring(v.c_type), '"\n')
            write('lang_type = "', tostring(v.lang_type), '"\n')
            depth = depth - 1
            write('}\n')
        elseif is_record(v) then
        elseif type(v) == 'function' then
        elseif type(v) == 'table' then
        else
            write(tostring(k),' = "', tostring(v), '"\n')
        end
    end
end,
unknown_end = function(self, rec, parent)
    depth = depth - 1
    write("}\n")
end,
c_source = function(self, rec, parent)
    local src = rec.src
    if type(src) == 'table' then src = table.concat(src) end
    write('c_source = "', src, '"\n')
end,
c_source_end = function(self, rec, parent)
end,
}

