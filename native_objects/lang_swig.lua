--
-- C to Lua Base types
--
basetype "bool"           "boolean"

basetype "char"           "integer"
basetype "unsigned char"  "integer"
basetype "short"          "integer"
basetype "unsigned short" "integer"
basetype "int"            "integer"
basetype "unsigned int"   "integer"
basetype "long"           "integer"
basetype "unsigned long"  "integer"
-- stdint types.
basetype "int8_t"         "integer"
basetype "int16_t"        "integer"
basetype "int32_t"        "integer"
basetype "int64_t"        "integer"
basetype "uint8_t"        "integer"
basetype "uint16_t"       "integer"
basetype "uint32_t"       "integer"
basetype "uint64_t"       "integer"

basetype "float"          "number"
basetype "double"         "number"

basetype "char *"         "string"
basetype "void *"         "lightuserdata"
basetype "void"           "nil"

local lua_base_types = {
    ['nil'] = { push = 'lua_pushnil' },
    ['number'] = { to = 'lua_tonumber', check = 'luaL_checknumber', push = 'lua_pushnumber' },
    ['integer'] = { to = 'lua_tointeger', check = 'luaL_checkinteger', push = 'lua_pushinteger' },
    ['string'] = { to = 'lua_tostring', check = 'luaL_checkstring', push = 'lua_pushstring' },
    ['boolean'] = { to = 'lua_toboolean', check = 'lua_toboolean', push = 'lua_pushboolean' },
    ['lightuserdata'] =
        { to = 'lua_touserdata', check = 'lua_touserdata', push = 'lua_pushlightuserdata' },
}

