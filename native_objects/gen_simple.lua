--
-- dump info
--
--[[
print"============ Dump types ================="
for k,v in pairs(types) do
    local lang_type = v.lang_type
    if lang_type == nil then
        lang_type = 'userdata'
    end
    print(v.c_type .. '\t(' .. k .. ')' .. '\tlua: ' .. lang_type)
end
]]
print"============ Dump objects ================="
local function find_ret(rec)
    for i=1,#rec do
        local v = rec[i]
        if is_record(v) and v._rec_type == 'var_out' then
            return v;
        end
    end
    return { c_type = "void" }
end
process_records{
object = function(self, rec, parent)
    print("object " .. rec.name .. "{")
    --print(dump(rec))
end,
property = function(self, rec, parent)
    print(rec.c_type .. " " .. rec.name .. "; /* is='" .. rec.is .. "', isa='" .. rec.isa .. "' */")
end,
include = function(self, rec, parent)
    print('#include "' .. rec.file .. '"')
end,
option = function(self, rec, parent)
    print("/* option: " .. rec.name .. " */")
end,
object_end = function(self, rec, parent)
    print("}\n")
end,
c_function = function(self, rec, parent)
    local ret = find_ret(rec)
    io.write(ret.c_type .. " " .. rec.name .. "(")
end,
c_function_end = function(self, rec, parent)
    print(")")
end,
var_in = function(self, rec, parent)
    if parent._first_var ~= nil then
        io.write(', ')
    else
        parent._first_var = true
    end
    io.write(rec.c_type .. " " .. rec.name)
end,
}

